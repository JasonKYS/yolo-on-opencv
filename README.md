# YOLO on OpenCV

This project is on using YOLO model on OpenCV in Python 3. In this project, YOLOv4-Tiny is used in the example. You can substitute the models with other pretrained models (https://github.com/AlexeyAB/darknet) or your own. This project is only using CPU to do the inference, if you wish to make use of GPU, please take a look at the other resources section as additional effort is required to compile the project for GPU which differs depending on your hardware.

# Initial setup

First use pip install to install the two prerequisite packages in the requirements.txt file. Then open the main.py script and substitute root_path to the path of your project directory and substitute video_path to your input video or replace with the value 0 if you want to use your webcam. You can then run the script which can start doing object detection using the pre-trained model.

# Other resources

Training YOLO model on Google Colab by Roboflow

https://colab.research.google.com/drive/1mzL6WyY9BRx4xX476eQdhKDnd_eixBlG#scrollTo=GNVU7eu9CQj3

Building the GPU version of OpenCV Python

https://www.pyimagesearch.com/2020/02/03/how-to-use-opencvs-dnn-module-with-nvidia-gpus-cuda-and-cudnn/

Checking GPU compability with CUDA and cuDNN for GPU based training and inference

https://docs.nvidia.com/deeplearning/cudnn/support-matrix/index.html
